L'approccio TDD è un modo di scrivere del codice che non era ancora
stato affrontato da noi studenti nel corso del trienno.
Quindi all'inzio mi sono trovato un po' in difficoltà a cambiare mentalità
nel modo di scrivere codice, essendo una modalità diametralmente opposta alla solita.
E' stata un'occasione interessante per conoscere l'approccio TDD che
può tornare utile in futuro.
Voto: 4.5/5