public class Main {
    public static String fizzbuzz(int n) {
        String ritorno="";
        for(int indice=1;indice<n+1;indice++) {
        if(indice%3==0 && indice%5==0)
                ritorno += "fizzbuzz";
            else if(indice%5==0)
                ritorno += "buzz";
            else if(indice%3==0)
                ritorno += "fizz";
            else if(indice%3!=0 && indice%5!=0)
                ritorno += indice;
        }
        return ritorno;
    }
}
