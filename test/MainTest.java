import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void fizzbuzz() {
        assertEquals("12fizz4buzzfizz78fizzbuzz11fizz1314fizzbuzz",Main.fizzbuzz(15));
    }
}